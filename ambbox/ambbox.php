<?php
/**
 * RCGarage Timing backend script
 *
 * Please read README.txt before using this tool
 *
 * @version 1.2.20171004
 * @author Andrey Monakhov <andrey.monakhov@gmail.com>
 */

error_reporting(E_ALL);
ini_set('display_errors', true);

$box = new RCGarageBox(__DIR__ . 'ambbox.php/' . (isset($argv[1]) ? ('ambconf/'.$argv[1].'.conf') : 'ambconf/main.conf'));
$box->start();

class RCGarageBox
{
    protected $configFile        = '';
    protected $config            = [];
    protected $conn              = null;
    protected $eventBuffer       = [];
    protected $recentEventBuffer = [];
    protected $serverTimeDiff    = null;
    protected $serverTime        = null;

    protected $online;

    public function __construct($configFile)
    {
        error_reporting(!E_ALL);

        $this->configFile = $configFile;
        $this->config     = parse_ini_file($configFile, true);

        register_shutdown_function([$this, 'close']);
    }
    
    public function start()
    {
        echo "Initialization using '{$this->configFile}'...\n";

        if (isset($this->config['proxy']['enable']) && $this->config['proxy']['enable']) {
            $this->startEthernet(true);
        } else {
            $protocol = isset($this->config['box']['protocol']) ? $this->config['box']['protocol'] : 'ethernet';
            $type     = isset($this->config['box']['decoderType']) ? $this->config['box']['decoderType'] : 'amb';

            switch ($type) {
                case 'rus-yl':
                    $this->startKrasnoyarskLap();
                    break;
                case 'amb':
                default:
                    switch ($protocol) {
                        case 'serial':
                            $this->startSerial();
                            break;
                        default:
                            $this->startEthernet();
                    }
            }
        }
    }

    protected function startEthernet($useProxy = false)
    {
        $boxAddress = $useProxy ? (isset($this->config['proxy']['host'])
            ? $this->config['proxy']['host']
            : '127.0.0.1') : $this->config['box']['ip'];
        $boxPort= $useProxy ? (isset($this->config['proxy']['port'])
            ? $this->config['proxy']['port']
            : 5100) : 5100;

        while(true) {
            while (!($this->conn = @fsockopen($boxAddress, $boxPort))) {
                echo "\rNo AMB Box connection. Connecting...\n";
                sleep(3);
            }

            echo "Connected to AMB Box\n";

            $this->online = false;

            if (!$this->sendStatus(true)) {
                echo "Internet connection lost. Working offline.\n";
                $this->online = false;
            } else {
                echo "Internet connection OK. Working online.\n";
                $this->online = true;
            }

            $counter = 0;
            $lastPortion = '';
            while ($buffer = fread($this->conn, 100)) {
                if ($lastPortion) {
                    $buffer = $lastPortion . $buffer;
                }
                $buffer = str_replace("\x01", '', $buffer);
                $buffer = trim($buffer, "\r\n");
                $buffer = explode("\r\n", $buffer);

                foreach ($buffer as $data) {
                    $lastPortion = $data;
                    $data = explode("\t", $data);

                    if (count($data) == 9 && $data[0] == '@' || count($data) == 6 && $data[0] == '#') {
                        $lastPortion = '';
                    } else {
                        continue;
                    }

                    if ($data[0] == '@') {
                        if (!$this->sendPassEvent($data)) {
                            if ($this->online) {
                                $this->online = false;
                                echo "\rInternet connection lost. Working offline.\n";
                            }
                        } else if (!$this->online) {
                            $this->online = true;
                            echo "\rInternet connection OK. Working online.\n";
                        }
                    } elseif ($data[0] == '#') {
                        if (!$this->sendStatus(true)) {
                            if ($this->online) {
                                $this->online = false;
                                echo "\rInternet connection lost. Working offline.\n";
                            }
                        } else if (!$this->online) {
                            $this->online = true;
                            echo "\rInternet connection OK. Working online.\n";
                        }
                    }

                    $counter++;
                    if ($counter < 4) {
                        echo '.';
                    } else {
                        $counter = 0;
                        echo "\r     \r";
                    }
                }
            }

            $this->sendStatus(false);
            echo "\rNo AMB Box connection. Connecting...\n";
        }
    }

    protected function startSerial()
    {
        $port = isset($this->config['box']['port']) ? $this->config['box']['port'] : 'COM4';

        while (($ret = $this->serialExec("mode $port BAUD=9600 PARITY=N DATA=8 TO=off XON=off STOP=1", $out)) !== 0) {
            echo "Error: No AMB Box Connection\n";
            sleep(3);
        }

        $this->conn = @fopen($port, 'r+b');
        if (!$this->conn) {
            echo "Error: No AMB Box Connection";
            die();
        }
        stream_set_blocking($this->conn, 0);

        // output status
        echo "Connected to AMB Box\n";

        $this->online = false;

        if (!$this->sendStatus(true)) {
            echo "Internet connection lost. Working offline.\n";
            $this->online = false;
        } else {
            echo "Internet connection OK. Working online.\n";
            $this->online = true;
        }

        // port initialization
        fwrite($this->conn, hex2bin('013f3b32303b303b33373b303b0d0a'));

        $buffer = '';
        while (true) {
            $data = fread($this->conn, 128);
            if ($data) {
                $data   = str_replace("\x01", '', $data);
                $buffer .= $data;
                $buffer = explode("\r\n", $buffer);

                foreach ($buffer as $i => $event) {
                    if ($i+1 == count($buffer)) {
                        $buffer = $event;
                        break;
                    }

                    $event = explode("\t", $event);

                    if ($event[0] == '@') {
                        if (!$this->sendPassEvent($event)) {
                            if ($this->online) {
                                $this->online = false;
                                echo "\rInternet connection lost. Working offline.\n";
                            }
                        } else if (!$this->online) {
                            $this->online = true;
                            echo "\rInternet connection OK. Working online.\n";
                        }
                    } elseif ($event[0] == '#') {
                        if (!$this->sendStatus(true)) {
                            if ($this->online) {
                                $this->online = false;
                                echo "\rInternet connection lost. Working offline.\n";
                            }
                        } else if (!$this->online) {
                            $this->online = true;
                            echo "\rInternet connection OK. Working online.\n";
                        }
                    }
                }
            }

            usleep(500000);
        }
    }

    public function startKrasnoyarskLap() {
        $port = isset($this->config['box']['port']) ? $this->config['box']['port'] : 'COM4';

        /*$descriptorspec = array(
            0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
            1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
            2 => array("pipe", "r")   // stderr is a file to write to
        );

        while (!($_handle = @proc_open($port, $descriptorspec, $pipes))) {
            echo "Error: No AMB Box Connection\n";
            sleep(3);
        }

        $this->conn  = $pipes[1];
        $this->wconn = $pipes[0];
        */

        while (($ret = $this->serialExec("mode $port BAUD=115200 PARITY=N DATA=8 TO=off XON=off STOP=1", $out)) !== 0) {
            echo "Error: Could Not Initiate Timing Box Connection\n";
            sleep(3);
        }

        $this->conn = @dio_open($port, O_RDWR);
        if (!$this->conn) {
            die("Error: No Timing Box Connection\n");
        }
        //stream_set_blocking($this->conn, 0);
        //stream_set_timeout($this->conn, 1);
        $this->wconn = $this->conn;

        // output status
        echo "Connected to Timing Box\n";

        $this->online = false;

        if (!$this->sendStatus(true)) {
            echo "Internet connection lost. Working offline.\n";
            $this->online = false;
        } else {
            echo "Internet connection OK. Working online.\n";
            $this->online = true;
        }

        // search for active competition event
        $competitions = $this->getData('Competitions');
        if (empty($competitions)) {
            die("No active competition events found. Exit.");
        }
        $competition = $competitions[0];

        echo "Found active competition #{$competition['id']}.\n";

        while ($heat = $this->getData('CompetitionUpcomingHeat', 'id=' . $competition['id'])) {
            if (empty($heat)) {
                break;
            }

            // get drivers for the active heat
            $drivers = $this->getData('CompetitionHeatDrivers', 'id=' . $heat['id']);
            if (empty($drivers)) {
                sleep(1);
                continue;
            }

            // push driver transponders into the box
            dio_write($this->wconn, "B\r");
            sleep(1);
            foreach ($drivers as $driver) {
                foreach ($driver['transponders'] as $transponder) {
                    echo "Uploading transponder #$transponder...\n";
                    dio_write($this->wconn, $transponder . "\r");
                    usleep(200000);
                }
            }
            dio_write($this->wconn, "#\r");
            sleep(1);

            echo "Checking that transponders are in place.\n";
            $buffer = '';
            $trCheck = false;
            while (true) {
                $data = dio_read($this->conn, 2);
                if ($data) {
                    $data = str_replace("\x01", '', $data);
                    $buffer .= $data;
                    $buffer = explode("\r", $buffer);

                    foreach ($buffer as $i => $trRow) {
                        $trRow = trim($trRow, "\n");
                        if (empty($trRow) || preg_match('/^(L:|TS\d|ETL)/i', $trRow)) {
                            unset($buffer[$i]);
                        }

                        if (preg_match('/ETL/i', $trRow)) {
                            $trCheck = true;
                        }
                    }

                    $buffer = implode("\r", $buffer);

                    if ($trCheck) {
                        break;
                    }
                }
            }

            // start listening to the box
            echo "Starting heat #{$heat['id']}\n";

            dio_write($this->wconn, "s\r");
            sleep(1);

            // wait for events to come in
            $buffer = '';
            $checkTime = time();
            while (true) {
                $data = dio_read($this->conn, 2);
                if ($data) {
                    $data = str_replace("\x01", '', $data);
                    $buffer .= $data;
                    $buffer = explode("\r", $buffer);

                    foreach ($buffer as $i => $event) {
                        $event = trim($event, " \n");
                        if ($i + 1 == count($buffer)) {
                            $buffer = $event;
                            break;
                        }

                        if (preg_match('/@(\d+)(\d\d)(\d\d)00/', $event, $eventDetails)) {
                            $event = [
                                0,0,0, // reserved
                                intval($eventDetails[1]),
                                $eventDetails[2] * 60 + $eventDetails[3],
                                0,
                                100,
                            ];

                            if (!$this->online && $this->sendStatus(true)) {
                                $this->online = true;
                                echo "\rInternet connection OK. Working online.\n";
                            }
                            
                            if (!$this->sendPassEvent($event)) {
                                if ($this->online) {
                                    $this->online = false;
                                    echo "\rInternet connection lost. Working offline.\n";
                                }
                            } else if (!$this->online) {
                                $this->online = true;
                                echo "\rInternet connection OK. Working online.\n";
                            }
                        }
                    }
                }

                // check for heat status (if finished -> break the loop)
                if (time() - $checkTime > 30) {
                    $checkTime = time();

                    $heat = $this->getData('CompetitionHeat', "id={$heat['id']}");
                    if (empty($heat) || $heat['status'] == 'finished') {
                        break;
                    }
                }
            }

            // finishing up the listener
            echo "Finishing up heat #{$heat['id']}\n";

            dio_write($this->wconn, "E\r");
            sleep(1);

            $buffer = '';
            while (true) {
                $buffer .= dio_read($this->conn, 2);
                if (preg_match('Stop', $buffer)) {
                    break;
                }
            }
        }

        echo "No more heats to run. Exit.\n";
    }

    public function close()
    {
        $this->sendStatus(false);
        if ($this->conn) {
            fclose($this->conn);
        }
    }

    /**
     * Sends online/offline status to the server.
     * Returns false if there is something wrong with the Internet connection
     *
     * @param bool $status true - online, false - offline
     *
     * @return bool
     */
    public function sendStatus($status)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->config['server']['url'] . 'ambbox/status',
            CURLOPT_HTTPHEADER => [
                'Accept: application/json',
                'X-Auth-Token: ' . $this->config['server']['token'],
            ],
            CURLOPT_POST, true,
            CURLOPT_POSTFIELDS => [
                'status' => $status ? 1 : 0
            ],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 2,
            CURLOPT_TIMEOUT => 4,
        ]);

        $result = curl_exec($curl);
        if (!$result) {
            return false;
        }

        $result = json_decode($result, true);
        if (!isset($result['status']) || !$result['status']) {
            return false;
        }

        // store server time difference with local time only once
        if (is_null($this->serverTimeDiff)) {
            $this->serverTime     = $result['ts'];
            $this->serverTimeDiff = $result['ts'] - time();
        }

        return true;
    }

    /**
     * Sends pass event to the server
     * Returns false if there is something wrong with the Internet connection
     *
     * @param array $event Raw output from the AMB box
     *
     * @return bool
     */
    public function sendPassEvent($event)
    {
        $curl = curl_init();
        $currentTime = time();

        $data = [
            'event'       => json_encode($event),
            'transponder' => $event[3],
            'timestamp'   => $event[4],
            'noise'       => $event[5],
            'signal'      => $event[6],
            'localts'     => $currentTime + $this->serverTimeDiff,
        ];

        // protection against occasional flood
        if (isset($this->recentEventBuffer[$data['transponder']])
            && $this->recentEventBuffer[$data['transponder']] + 2 > $currentTime
        ) {
            return true;
        }
        $this->recentEventBuffer[$data['transponder']] = $currentTime;

        $this->eventBuffer[] = $data;

        // nothing else to do when offline
        if (!$this->online) {
            return false;
        }

        if (count($this->eventBuffer) > 1) {
            curl_setopt_array($curl, [
                CURLOPT_URL => $this->config['server']['url'] . 'ambbox/batch',
                CURLOPT_HTTPHEADER => [
                    'Accept: application/json',
                    'X-Auth-Token: ' . $this->config['server']['token'],
                ],
                CURLOPT_POST, true,
                CURLOPT_POSTFIELDS => [
                    'batch' => serialize($this->eventBuffer)
                ],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
            ]);
        } else {
            curl_setopt_array($curl, [
                CURLOPT_URL => $this->config['server']['url'] . 'ambbox/event',
                CURLOPT_HTTPHEADER => [
                    'Accept: application/json',
                    'X-Auth-Token: ' . $this->config['server']['token'],
                ],
                CURLOPT_POST, true,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
            ]);
        }

        $result = curl_exec($curl);
        if (!$result) {
            return false;
        }

        $result = json_decode($result, true);
        if (!isset($result['status']) || !$result['status']) {
            return false;
        }

        // flash offline buffer when there is everything ok
        $this->eventBuffer = [];

        return true;
    }

    /**
     * Makes a request to the API server and returns the results
     *
     * @return mixed
     */
    public function getData($endPoint, $queryString = '')
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->config['server']['url'] . $endPoint . ($queryString ? '?' . $queryString : ''),
            CURLOPT_HTTPHEADER => [
                'Accept: application/json',
                'X-Auth-Token: ' . $this->config['server']['token'],
            ],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 2,
            CURLOPT_TIMEOUT => 4,
        ]);

        $result = curl_exec($curl);
        if (!$result) {
            return false;
        }

        $result = json_decode($result, true);
        return isset($result['data']) ? $result['data'] : false;
    }

    protected function serialExec($cmd, &$out = null)
    {
        $desc = array(
            1 => array("pipe", "w"),
            2 => array("pipe", "w")
        );

        $proc = proc_open($cmd, $desc, $pipes);

        $ret = stream_get_contents($pipes[1]);
        $err = stream_get_contents($pipes[2]);

        fclose($pipes[1]);
        fclose($pipes[2]);

        $retVal = proc_close($proc);

        if (func_num_args() == 2) {
            $out = array($ret, $err);
        }

        return $retVal;
    }
}