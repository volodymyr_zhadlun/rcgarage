<?php
/**
 * RCGarage Timing proxy script
 *
 * Makes two different scoring system work together
 *
 * @version 1.0.20170819
 * @author Andrey Monakhov <andrey.monakhov@gmail.com>
 */

$proxy = new RCGarageTimingProxy(__DIR__ . 'proxy.php/' . (isset($argv[1]) ? ('ambconf/'.$argv[1].'.conf') : 'ambconf/main.conf'));

try {
    $proxy->start();
} catch (RCGarageException $e) {
    echo $e->getMessage();
}

class RCGarageTimingProxy
{
    protected $proxyPort     = 5100;
    protected $configFile    = '';
    protected $config        = [];
    protected $boxConnection = null;
    protected $listeners     = [];

    private $proxySocket   = null;

    public function __construct($configFile)
    {
        error_reporting(!E_ALL);

        $this->configFile = $configFile;
        $this->config     = parse_ini_file($configFile, true);

        register_shutdown_function([$this, 'close']);
    }


    public function start()
    {
        $this->proxySocket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        if (!$this->proxySocket) {
            throw new RCGarageException('Could not create RCGarage Proxy');
        }
        if (!socket_bind($this->proxySocket, isset($this->config['proxy']['host']) ? $this->config['proxy']['host'] : '127.0.0.1',
            isset($this->config['proxy']['port']) ? $this->config['proxy']['port'] : $this->proxyPort)
        ) {
            throw new RCGarageException('Could not start RCGarage Proxy');
        }

        $this->log("Proxy started. Waiting for connections...");
        if (!socket_listen($this->proxySocket)) {
            throw new RCGarageException('RCGarage Proxy unexpected stop.');
        }

        for ($i = 0; $i < 2; $i++) {
            $this->listeners[$i] = socket_accept($this->proxySocket);

            $this->log(($i == 0 ? '1st' : '2nd') . " listener is connected");
        }

        $this->log("Proxy is working...");

        if (isset($this->config['box']['protocol']) && $this->config['box']['protocol'] == 'serial') {
            $this->startSerial();
        } else {
            $this->startEthernet();
        }

        $this->log("Exit.");
    }

    protected function startEthernet()
    {
        while(true) {
            while (!($this->boxConnection = fsockopen($this->config['box']['ip'], 5100))) {
                echo "\rNo Timing Box connection. Connecting...\n";
                sleep(3);
            }

            $this->log("Connected to Timing Box");

            while ($buffer = fread($this->boxConnection, 100)) {
                foreach ($this->listeners as $listener) {
                    if (!socket_write($listener, $buffer)) {
                        echo socket_last_error($listener), "\n";
                    }
                }
            }

            echo "\rNo Timing Box connection. Connecting...\n";
            fclose($this->boxConnection);
        }
    }

    protected function startSerial()
    {
        $port = isset($this->config['box']['port']) ? $this->config['box']['port'] : 'COM4';

        while (($ret = $this->serialExec("mode $port BAUD=9600 PARITY=N DATA=8 TO=off XON=off STOP=1", $out)) !== 0) {
            echo "Error: No Timing Box Connection\n";
            sleep(3);
        }

        $this->boxConnection = @fopen($port, 'r+b');
        if (!$this->boxConnection) {
            echo "Error: No Timing Box Connection";
            die();
        }
        stream_set_blocking($this->boxConnection, 0);

        // output status
        $this->log("Connected to Timing Box");

        // port initialization
        fwrite($this->boxConnection, hex2bin('013f3b32303b303b33373b303b0d0a'));

        while (true) {
            $data = fread($this->boxConnection, 128);
            if ($data) {
                foreach ($this->listeners as $listener) {
                    fwrite($listener, $data);
                }
            }
        }
    }

    protected function serialExec($cmd, &$out = null)
    {
        $desc = array(
            1 => array("pipe", "w"),
            2 => array("pipe", "w")
        );

        $proc = proc_open($cmd, $desc, $pipes);

        $ret = stream_get_contents($pipes[1]);
        $err = stream_get_contents($pipes[2]);

        fclose($pipes[1]);
        fclose($pipes[2]);

        $retVal = proc_close($proc);

        if (func_num_args() == 2) {
            $out = array($ret, $err);
        }

        return $retVal;
    }

    public function close()
    {

    }

    protected function log($message)
    {
        echo $message, "\n";
    }
}

class RCGarageException extends Exception
{

}